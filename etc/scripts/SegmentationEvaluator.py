#!/usr/bin/python
import argparse
import glob
from numpy.dual import norm
import os, sys


from lxml import etree
from matplotlib.pyplot import figure, show, cm
from matplotlib import pyplot
import matplotlib as mpl
import numpy

__author__ = 'waechter'

def evaluateMotionFiles(path, mainObjectName):
    segmentations = get_MMM_Files(path)
    segmentations = sorted(segmentations)
    resultMap = {}
    evaluatedMotionCount = 0

    for seg in segmentations:
        # print seg
        evaluatedMotionCount += compareMotion(seg, resultMap, mainObjectName)
        # print "\n"
    # print "Overall results"
    normalizedResults = {}
    for result in resultMap:
        normalizedResults[result] = [x / evaluatedMotionCount for x in resultMap[result]]
    print normalizedResults
    return normalizedResults

def evaluateMotion(multikeyframes, mainObjectName, MMMFilepath):
    resultMap = {}
    evaluatedMotionCount = 0

    segmentations = get_MMM_Files(MMMFilepath)
    segmentations = sorted(segmentations)

    for seg in multikeyframes:
        # print seg
        evaluatedMotionCount += compareMotion(seg, resultMap, mainObjectName)
        # print "\n"
    # print "Overall results"
    normalizedResults = {}
    for result in resultMap:
        normalizedResults[result] = [x / evaluatedMotionCount for x in resultMap[result]]
    print normalizedResults
    return normalizedResults

def main():

    parser = argparse.ArgumentParser(description="Segmentation evaluation script that creates a score for a segmentation"
                                                 "compared to a ground truth segmentation.")
    # parser.add_argument("groundTruthSegmentation", help='Path to the ground truth segmentation')
    parser.add_argument("--testFile", "-t", help='Path to the test segmentation', dest="testFile")
    parser.add_argument("--mainObjectName", "-o", help='Name of the mainobject for the segmentation', required=True)


    args = parser.parse_args()
    evaluateMotionFiles(args.testFile, args.mainObjectName)


def plot_keyframes_barcode(groundTruthKeyFrames, missedKeyframes, testKeyFrames, unmatchedKeyframes, algoname, filename):

    if len(testKeyFrames) == 0:
        return
    fig = figure(figsize=(9,2))

    z = [x*10 for x in groundTruthKeyFrames]

    groundTruthKeyFramesCopy = groundTruthKeyFrames
    testKeyFramesCopy = testKeyFrames
    maxval = 0
    if len(testKeyFrames) > 0:
        maxval = max(testKeyFrames[-1], groundTruthKeyFrames[-1])
    if len(unmatchedKeyframes) > 0:
        maxval = max(maxval, unmatchedKeyframes[-1])
    if len(missedKeyframes) > 0:
        maxval = max(maxval, missedKeyframes[-1])
    i = 0.0
    x = []
    x2 = []
    while i <= maxval+0.1:
        x.append([1,1,1])
        x2.append([1,1,1])
        i += 0.1
    for kf in groundTruthKeyFrames:
        x[int(kf*10)] = [0,0,0]
    for kf in missedKeyframes:
        x[int(kf*10)] = [1,0,0]
    for kf in testKeyFrames:
        x2[int(kf*10)] = [0,1,0]
    for kf in unmatchedKeyframes:
        x2[int(kf*10)] = [0,0,0]

    x = numpy.array(x)
    x.shape = 1, len(x),3
    x2= numpy.array(x2)
    x2.shape = 1,len(x2),3
    # x.shape = 1, len(x)
    ax = fig.add_axes([0.01, 0.6, 0.95, 0.25], xticks=[], yticks=[])
    ax2 = fig.add_axes([0.01, 0.2, 0.95, 0.25], xticks=[maxval*10], yticks=[])
    cmap = mpl.colors.ListedColormap(['w', "g", "r"])
    ax.imshow(x, aspect='auto', cmap=cmap, interpolation='nearest')
    ax2.imshow(x2, aspect='auto', cmap=cmap, interpolation='nearest')

    ax.set_title("Manual Segmentation", fontsize=18)
    ax2.set_title(algoname, fontsize=18)
    label = str(maxval) + " s"
    ax.set_xticklabels([label])
    ax2.set_xticklabels([label])

    # pyplot.show()
    filesplit = os.path.splitext(filename)

    picFile = filesplit[0] + "_" + algoname.replace (" ", "_") + ".pdf"
    pyplot.savefig(picFile)

def plot_keyframes(groundTruthKeyFrames, testKeyFrames, algoname, filename):

    if len(testKeyFrames) == 0:
        return
    # Make a figure and axes with dimensions as desired.
    fig = pyplot.figure(figsize=(7,2))
    ax1 = fig.add_axes([0.01, 0.7, 0.98, 0.25])
    ax2 = fig.add_axes([0.01, 0.27, 0.98, 0.25], xticks=groundTruthKeyFrames)

    groundTruthKeyFramesCopy = groundTruthKeyFrames
    testKeyFramesCopy = testKeyFrames
    if groundTruthKeyFramesCopy.count(0) > 0:
        groundTruthKeyFramesCopy.remove(0)
    if testKeyFramesCopy.count(0) > 0:
        testKeyFramesCopy.remove(0)


    cmap = mpl.colors.ListedColormap(['w'])

    cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=None,
                                         norm=None,
                                         # to use 'extend', you must
                                         # specify two extra boundaries:
                                         boundaries=groundTruthKeyFramesCopy,
                                         # ticks=groundTruthKeyFramesCopy, # optional
                                         ticks = groundTruthKeyFramesCopy,
                                         spacing='proportional',
                                         orientation='horizontal')

    cb1.set_label("Manual Segmentation")
    print testKeyFrames

    # norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=None,
                                         norm=None,
                                         # to use 'extend', you must
                                         # specify two extra boundaries:
                                         boundaries=testKeyFramesCopy,
                                         # ticks=testKeyFramesCopy, # optional
                                         ticks = testKeyFramesCopy,
                                         spacing='proportional',
                                         orientation='horizontal')
    cb2.set_label(algoname)
    ax1.set_xticklabels([])
    ax2.set_xticklabels([])

    # pyplot.show()
    filesplit = os.path.splitext(filename)

    picFile = filesplit[0] + "_" + algoname.replace (" ", "_") + ".pdf"
    pyplot.savefig(picFile)

def read_segmentation_xml(xmlFile, objectName):
    result = {}
    # print "Reading " + xmlFile
    if not os.path.exists(xmlFile):
        # print "File " + xmlFile + " does not exist!"
        return result
    tree = etree.parse(xmlFile)
    root = tree.getroot()
    xpath = "algorithm"
    for e in root.findall(xpath):
        subxpath = "*[@name='" + objectName + "']/frame"
        keyframes = []
        for sub in e.findall(subxpath):
            keyframes.append(float(sub.text))
        # print "attrib: " + e.attrib.get("name")
        result[e.attrib.get("name")] = keyframes
    # print result
    return result






def get_MMM_Files(motionFilePath):
    motions = []
    if os.path.isfile(motionFilePath):
        print "Single file: " + motionFilePath
        motions.append(motionFilePath)
    else:
        motions = glob.glob(motionFilePath+ "/*.xml")
        print "Batch mode: "
        print motions

    return motions


def printKeyframes(groundTruthKeyFrames):
    visu = ""
    last = 0
    for gtKf in groundTruthKeyFrames:
        for x in range(int(round(last)), int(round(gtKf) - 1)):
            visu += "."
        visu += "|"
        last = gtKf
    return visu


def evaluate_keyframes(groundTruthKeyFrames, testKeyFrames, algoName, motionKeyframeFile):


    error = 0
    accuracy = 0
    maxDistance = 1
    missedKeyframesPenalty = 7
    missedKeyframesNumber = 0
    falsePositiveKeyframesPenalty = 7
    unmatchedKeyframesNumber = len(testKeyFrames)
    missedKeyframes = []
    matchedKeyframes = []
    # print groundTruthKeyFrames
    # print printKeyframes(groundTruthKeyFrames)
    # print printKeyframes(testKeyFrames)
    # print testKeyFrames
    for gtKf in groundTruthKeyFrames:
        bestDistance = sys.float_info.max
        i = 0
        bestKeyframe = -1
        for testKf in testKeyFrames:
            distance = abs(testKf-gtKf)
            if distance < bestDistance:
                bestDistance = distance
                bestKeyframe = i
            i += 1
        if bestDistance < maxDistance:
            error += bestDistance * bestDistance
            accuracy += bestDistance
            unmatchedKeyframesNumber -= 1
            matchedKeyframes.append(testKeyFrames[bestKeyframe])
            testKeyFrames.pop(bestKeyframe)
        else:
            error += missedKeyframesPenalty
            missedKeyframes.append(gtKf)
            missedKeyframesNumber += 1
    error += falsePositiveKeyframesPenalty * unmatchedKeyframesNumber
    # plot_keyframes_barcode(groundTruthKeyFrames, missedKeyframes, matchedKeyframes, testKeyFrames, algoName, motionKeyframeFile)
    accuracyNormalized = accuracy
    if (len(groundTruthKeyFrames)-missedKeyframesNumber) > 0:
        accuracyNormalized = accuracy/(len(groundTruthKeyFrames)-missedKeyframesNumber);
    return [error/len(groundTruthKeyFrames), accuracyNormalized, unmatchedKeyframesNumber, missedKeyframesNumber]



#
# root = etree.Element("root")
#
# objectXML = etree.SubElement(root, "object", name="RedCup")
# etree.SubElement(objectXML, "frame").text = "123"
# # tree = ET.ElementTree(root)
# print etree.tostring(root, pretty_print=True)
# tree = etree.ElementTree(root)
# tree.write("/tmp/myxml.xml", pretty_print=True)


def compareKeyframeArrays(gtKeyframes, multiAlgoKeyframes, motionKeyframeFile, resultMap):
    if len(gtKeyframes) == 0:
        return False
    # print "Comparing:\n Groundtruth \n\t" + gtFile + "\n with testdata \n\t" + motionKeyframeFile
    for algo in multiAlgoKeyframes:
        testKeyframes = multiAlgoKeyframes[algo]
        # plot_keyframes_barcode(gtKeyframes["GroundTruth"], testKeyframes, algo, motionKeyframeFile)
        result = evaluate_keyframes(gtKeyframes["GroundTruth"], testKeyframes, algo, motionKeyframeFile)

        if not algo in resultMap:
            resultMap.setdefault(algo, [0,0,0,0])
        resultMap[algo][0] += result[0]
        resultMap[algo][1] += float(result[1])
        resultMap[algo][2] += float(result[2])
        resultMap[algo][3] += float(result[3])
        # print algo, result
    return True

def compareMotion(motionKeyframeFile, resultMap, mainObjectName):
    motionCompareCount = 0
    filesplit = os.path.splitext(motionKeyframeFile)
    gtFile = filesplit[0] + ".gtkfxml"
    gtKeyframes = read_segmentation_xml(gtFile, mainObjectName)
    multiAlgoKeyframes = read_segmentation_xml(motionKeyframeFile, mainObjectName)
    if compareKeyframeArrays(gtKeyframes, multiAlgoKeyframes, motionKeyframeFile, resultMap):
        motionCompareCount += 1

    gtFile = filesplit[0] + "_peter.gtkfxml"
    gtKeyframes = read_segmentation_xml(gtFile, mainObjectName)
    multiAlgoKeyframes = read_segmentation_xml(motionKeyframeFile, mainObjectName)
    if compareKeyframeArrays(gtKeyframes, multiAlgoKeyframes, motionKeyframeFile, resultMap):
        motionCompareCount += 1

    return motionCompareCount

def get_MMM_Files(segmentationFilePath):
    motions = []
    if os.path.isfile(segmentationFilePath):
        filesplit = os.path.splitext(segmentationFilePath)
        segmentationFilePath = filesplit[0] + ".kfxml"
        # print "Single file: " + segmentationFilePath
        motions.append(segmentationFilePath)
    else:
        motions = glob.glob(segmentationFilePath+ "/*.kfxml")
        # print "Batch mode: "
        # print motions

    return motions

if __name__ == "__main__":

    main()
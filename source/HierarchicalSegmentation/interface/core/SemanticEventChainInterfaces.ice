/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    HierarchicalSegmentation::Core
* @author     Mirko Waechter <waechter @ kit . edu>
* @copyright  2014 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _HIERARCHICALSEGMENTATION_CORE_SEMANTIC_EVENT_CHAIN_INTERFACES_SLICE_
#define _HIERARCHICALSEGMENTATION_CORE_SEMANTIC_EVENT_CHAIN_INTERFACES_SLICE_
#ifndef MININTERFACE
#include <MemoryX/interface/memorytypes/MemoryEntities.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#endif
/**
 * Definition of all SemanticEventChain interfaces
 */
module hierarchicalsegmentation
{
    sequence <string> StringList;
    sequence <double> Keyframes;
    dictionary <string, Keyframes> ObjectKeyframes;
    dictionary <string, ObjectKeyframes> SegmentationResultList;
    sequence <double> DoubleList;

    enum SemanticConfig {
        eDistanceThreshold,
        eSemanticMergeThreshold,
        eHystereseFactor,
        eMinSegmentSize,
        eWindowSize,
        eDivideThreshold,
        eDerivation,
        eWeightRoot,
        eGaussFilterWidth,
        eSemanticConfigParamCount
    };

    struct Relation
    {
        string type;
        string object1;
        string object2;
    };

    sequence<Relation> RelationList;
    struct SimpleWorldState
    {
        RelationList relations;
        double timestamp;
    };
    dictionary<int, SimpleWorldState> KeyframesWithWorldState;
    sequence<SimpleWorldState> KeyframesWithWorldStateVec;
    interface SemanticEventChainGeneratorInterface
    {
#ifndef MININTERFACE
        memoryx::SECKeyFrameMap calculateSEC(StringList viconCSVString);        
#endif
        KeyframesWithWorldStateVec getWorldStates(string MMMFilePath, string objectName, DoubleList params);
        KeyframesWithWorldStateVec calcWorldStatesAtFrames(string MMMFilePath, DoubleList frames, DoubleList params);
        ObjectKeyframes calcKeyframes(string MMMFilePath);
        ObjectKeyframes calcKeyframesWithConfig(string MMMFilePath, DoubleList params);
        Keyframes getXPositions(string MMMFilePath, string objectName);
    };

#ifndef MININTERFACE
    interface SemanticEventChainInterpreterInterface
    {

        memoryx::SECMotionSegmentList findOACSequence(memoryx::SECKeyFrameMap secs);
    };
#endif

    interface MultiSegmenterInterface
    {
        SegmentationResultList segment(string MMMFilePath, string mainObjectName);
    };


};

#endif

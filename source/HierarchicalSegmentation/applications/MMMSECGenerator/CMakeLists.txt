armarx_component_set_name("MMMSECGeneratorApp")

#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
#
# all include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    include_directories(${MyLib_INCLUDE_DIRS})
#endif()

find_package(DMP QUIET)
armarx_build_if(DMP_FOUND "DMP not available")
LINK_DIRECTORIES(${DMP_LIB_DIRS})

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore MMMSECGenerator)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticSegmentationEvaluator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE HierarchicalSegmentation::ArmarXObjects::SemanticSegmentationEvaluator

#define ARMARX_BOOST_TEST

#include <HierarchicalSegmentation/Test.h>
#include <HierarchicalSegmentation/components/SemanticSegmentationEvaluator/SemanticSegmentationEvaluator.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(testTooFewKeyFrames)
{
    armarx::SemanticSegmentationEvaluator instance;

    std::vector<double> groundTruth{1,2,3};
    std::vector<double> test{1,3};
    auto error = instance.calculateMetric(test, groundTruth);
    ARMARX_INFO_S << VAROUT(error);
    BOOST_CHECK_EQUAL(error, 2);
}


BOOST_AUTO_TEST_CASE(testTooManyKeyFrames)
{
    armarx::SemanticSegmentationEvaluator instance;

    std::vector<double> groundTruth{1,2,3};
    std::vector<double> test{1,2,3,4};
    auto error = instance.calculateMetric(test, groundTruth);
    ARMARX_INFO_S << VAROUT(error);
    double desiredresult = 5.0/3;
    BOOST_CHECK_EQUAL(error, desiredresult);
}


BOOST_AUTO_TEST_CASE(halfOff)
{
    armarx::SemanticSegmentationEvaluator instance;

    std::vector<double> groundTruth{1,2,3};
    std::vector<double> test{0.5,1.5,3.5};
    auto error = instance.calculateMetric(test, groundTruth);
    ARMARX_INFO_S << VAROUT(error);
    BOOST_CHECK_EQUAL(error, 0.25);
}

BOOST_AUTO_TEST_CASE(testTooFewKeyFrames2)
{
    armarx::SemanticSegmentationEvaluator instance;

    std::vector<double> groundTruth{1,2,3};
    std::vector<double> test{1,4};
    auto error = instance.calculateMetric(test, groundTruth);
    ARMARX_INFO_S << VAROUT(error);
    BOOST_CHECK_EQUAL(error, 2);
}


# Libs required for the tests
LINK_DIRECTORIES(${DMP_LIB_DIRS})
SET(LIBS ${LIBS} ArmarXCore SemanticEventChainInterpreter SemanticEventChainGenerator MemoryXMemoryTypes LongtermMemory PriorKnowledge CommonStorage)

# test cannot be found, removed
#armarx_add_test(SemanticEventChainInterpreterTest SemanticEventChainInterpreterTest.cpp "${LIBS}")

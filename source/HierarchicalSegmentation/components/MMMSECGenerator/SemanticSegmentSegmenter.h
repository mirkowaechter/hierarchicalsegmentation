/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ARMARX_SEMANTICSEGMENTSEGMENTER_H
#define ARMARX_SEMANTICSEGMENTSEGMENTER_H

#include <dmp/segmentation/characteristicsegmentation.h>
#include <MMM/Motion/Motion.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>

#include "ModelBasedTaskSegmentation.h"

namespace armarx {

    class SemanticSegmentSegmenter
    {
    public:
        typedef std::map<std::pair< std::string, int>, std::vector<int> > NameSegmentValuesMap;

        SemanticSegmentSegmenter(float windowSize,
                                 float minSegmentSize,
                                 float divideThreshold,
                                 float derivation,
                                 float weightRoot,
                                 float gaussFilterWidth);
        void setMotion(MMM::MotionPtr motion);
        void setMotions(ModelBasedTaskSegmentation::MotionDataMap motions);
        void setMotions(const MMM::MotionList &motions);
        void setWorldStates(memoryx::SECKeyFrameMap worldStates);
        void setSemanticKeyframes(const NameIntSetMap &secKeyframesByObject);

        void calcKeyframes();
        NameSegmentValuesMap getSubKeyframes() const { return subKeyframes;}
        const std::map<std::string, DMP::SemanticSegmentData>&  getCompleteSegmentationData() const;
        memoryx::SECKeyFrameMap convertToFlatSegmentation() const;
        std::map<std::string, std::vector<double> > getAllKeyFrames() const;
    protected:
        NameIntSetMap secKeyframesByObject;
        ModelBasedTaskSegmentation::MotionDataMap motions;
        NameSegmentValuesMap subKeyframes;
        std::map<std::string, DMP::SemanticSegmentData> segData;
        memoryx::SECKeyFrameMap worldStates;

        float windowSize;
        float minSegmentSize;
        float divideThreshold;
        float derivation;
        float weightRoot;
        float gaussFilterWidth;
    };

}

#endif

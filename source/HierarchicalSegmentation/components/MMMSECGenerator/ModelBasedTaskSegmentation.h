/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ARMARX_MODELBASEDTASKSEGMENTATION_H
#define ARMARX_MODELBASEDTASKSEGMENTATION_H

#include <ArmarXCore/core/logging/Logging.h>
#include <MMM/Motion/Motion.h>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>


namespace VirtualRobot
{
    class CDManager;
    typedef boost::shared_ptr<CDManager> CDManagerPtr;
    class Robot;
    typedef boost::shared_ptr<Robot> RobotPtr;
}

namespace armarx {

    typedef std::map<std::string, std::set<int> > NameIntSetMap;

    class ModelBasedTaskSegmentation :
            public Logging
    {
    public:
        ModelBasedTaskSegmentation(const MMM::MotionList &motions, float collisionThreshold = 5.0, float mergeThreshold = 40, float hystereseFactor = 3);
        void setObjects(const memoryx::ObjectClassList &objects);
        void calc();
        void plot();
        memoryx::SECKeyFrameMap getKeyFrames() const;
        std::set<int> getKeyFramesForObject(const std::string &objName) const;
        NameIntSetMap getKeyFramesGroupedByObject() const;

        memoryx::SECKeyFrameMap calcWorldStatesAtTimestamps(const Ice::DoubleSeq timestamps) const;

        struct MotionData
        {
            MMM::MotionPtr motion;
            VirtualRobot::RobotPtr model;
        };
        typedef std::map<std::string, MotionData> MotionDataMap;
        const MotionDataMap& getMotionData() const { return _motions;}
    protected:
        void _findKeyFrames();
        void _findKeyFrames(MotionDataMap::iterator obj1, MotionDataMap::iterator obj2, memoryx::SECKeyFrameMap& outputKeyFrames);
        VirtualRobot::CDManager _prepareCollisionDetection(MotionDataMap::const_iterator obj1, MotionDataMap::const_iterator obj2) const;
        memoryx::ObjectClassBasePtr _getObject(const std::string &objectName) const;
        memoryx::SECKeyFrameMap calcCumulativeWorldStates();
        void updateKeyframeGroupedByObject();

    protected:
//        MMM::MotionList _motions;
        MotionDataMap _motions;
        memoryx::SECKeyFrameMap _keyframes;
        NameIntSetMap _objectGroupedKeyFramekeyframes;
        VirtualRobot::CDManagerPtr cdMan;
        memoryx::ObjectClassMap _objects;
        std::set<std::pair<std::string, std::string> > _objectsWithContact;
        std::map<std::pair<std::string, std::string>, std::vector<double> > _objectDistances;

        float collisionThreshold;
        float mergeThreshold;
        float hystereseFactor;
    };

} // namespace armarx

#endif

/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ARMARX_VICONCSVPARSER_H
#define ARMARX_VICONCSVPARSER_H

#include <vector>
#include <string>
#include <limits>
#include <map>

namespace armarx {

    typedef std::vector<double>  CSVRow;
    class ViconCSVParser
    {
    public:
        ViconCSVParser();

        void readFromCSVFile(const std::string &__filepath, bool doParsing = true, unsigned int linesToSkip = 0, unsigned int lineCount = std::numeric_limits<unsigned int>::max());
        void readFromString(const std::vector<std::string> &viconCSVLines, unsigned int linesToSkip = 0, unsigned int lineCount = std::numeric_limits<unsigned int>::max());
        const std::vector<CSVRow>& getRows() const { return __rows;}

        void convertToCartesianVectors(std::vector<std::vector<std::vector<double> > > & _3DPosMultiSequence, int startColumn = 0) const;
        const std::vector<std::string>& getLines() { return __lines;}
        std::vector<double>  getColumn(unsigned int colIndex) const;
        const std::map<std::string, std::vector<int> >& getMarkerGroups() const{ return __markerGroups;}
    protected:
        void _parseMarkerGroups();
    private:
        std::string __filepath;
        std::vector<std::string> __lines;
        std::vector<CSVRow> __rows;
        std::map<std::string, std::vector<int> > __markerGroups;
    };

} // namespace armarx

#endif // ARMARX_VICONCSVPARSER_H

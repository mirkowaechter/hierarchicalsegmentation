/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "ViconCSVParser.h"

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <HierarchicalSegmentation/libraries/SemanticEventChains/VectorOperations.h>


#include <fstream>
#include <boost/algorithm/string/trim.hpp>
#include <boost/tokenizer.hpp>

namespace armarx {

    ViconCSVParser::ViconCSVParser()
    {
    }

    void ViconCSVParser::readFromCSVFile(const std::string &filepath, bool doParsing, unsigned int linesToSkip, unsigned int lineCount)
    {

        std::ifstream f (filepath.c_str());
        if(!f.is_open())
            throw LocalException("Could not open file: " + filepath);
        this->__filepath = filepath;
        __rows.clear();
        std::cout << "Reading from file : " << filepath << std::endl;
        std::string line;

        unsigned int lineIndex=1;


        __lines.clear();
        while (getline(f,line) && lineIndex <= lineCount)
        {
            __lines.push_back(line);
            lineIndex++;
        }
        ARMARX_VERBOSE_S << "read " << __lines.size() << " from file";
        if(doParsing)
            readFromString(__lines, linesToSkip, lineCount);
    }

    void ViconCSVParser::readFromString(const std::vector<std::string> &viconCSVLines, unsigned int linesToSkip, unsigned int lineCount)
    {

        if(&__lines != &viconCSVLines)
            __lines = viconCSVLines;

        unsigned int lineIndex=0;
        for(std::vector<std::string>::iterator it = __lines.begin();  it != __lines.end(); it++)
        {
            if(lineIndex < linesToSkip)
            {
                lineIndex++;
                continue;
            }

            if(lineIndex >= lineCount)
                break;

            using namespace boost;
            typedef tokenizer< escaped_list_separator<char> > Tokenizer;
            std::string& line = *it;
            boost::algorithm::trim(line);

            Tokenizer tok(line);
            std::vector<std::string> stringVec;
            stringVec.assign(tok.begin(),tok.end());
            if(stringVec.size() == 0)
                continue;
            __rows.push_back(CSVRow(stringVec.size(),0.0));
            for (unsigned int col = 0; col < stringVec.size(); ++col) {

                std::string& str = stringVec.at(col);
                boost::algorithm::trim(str);
                if(str.find("[") != std::string::npos)
                    str = str.replace(str.find("["), 2, "");
                if(str.find("]") != std::string::npos)
                    str = str.replace(str.find("]"), 2, "");
                if(str.find("  ") != std::string::npos)
                    str = str.replace(str.find("  "), 2, " ");

                CSVRow& row = (*__rows.rbegin());
                row.at(col) = fromString<double>(stringVec.at(col));
            }
            lineIndex++;
        }

        ARMARX_VERBOSE_S << "parsed " << __lines.size() << "";
        _parseMarkerGroups();
    }

    void ViconCSVParser::convertToCartesianVectors(std::vector<std::vector<std::vector<double> > > &_3DPosMultiSequence, int startColumn) const
    {
        _3DPosMultiSequence.clear();
        _3DPosMultiSequence.resize(__rows.size());
        for (unsigned int rowIndex = 0; rowIndex < __rows.size(); ++rowIndex) {
            const std::vector<double> & row = __rows.at(rowIndex);
            for (unsigned int col = startColumn; col < row.size(); col+=3){
                std::vector<double>  pos(row.begin()+col, row.begin()+col+3);
                _3DPosMultiSequence.at(rowIndex).push_back(pos);
            }
        }
    }

    std::vector<double> ViconCSVParser::getColumn(unsigned int colIndex) const
    {
        std::vector<double>  result;
        for (unsigned int row = 0; row < __rows.size(); ++row) {
            result.push_back(__rows.at(row).at(colIndex));
        }
        return result;
    }

    void ViconCSVParser::_parseMarkerGroups()
    {
        __markerGroups.clear();
        using namespace boost;
        typedef tokenizer< escaped_list_separator<char> > Tokenizer;


        std::string line;
        // skipp first two lines
        if(__lines.size() < 3)
        {
            ARMARX_WARNING_S << "Could not read marker groups because there are only " << __lines.size() << " lines";
            return;
        }
        line = __lines.at(2);
        Tokenizer tok(line);
        std::vector<std::string> stringVec(tok.begin(),tok.end());
        unsigned int col = 2;

        for(; col < stringVec.size(); col++)
        {
            const std::string& content = stringVec.at(col);
            std::string::size_type pos = content.find(":");
            if(pos != std::string::npos)
            {
                std::string groupName = content.substr(0, pos);
                int markerIndex = col/3;
                std::cout << "GroupName: " << groupName << " marker index: " << markerIndex <<  std::endl;
                __markerGroups[groupName].push_back(markerIndex);
            }
        }

    }

} // namespace armarx

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticEventChainGenerator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticEventChainGenerator_H
#define _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticEventChainGenerator_H


#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/component/PriorKnowledgeInterface.h>
#include <HierarchicalSegmentation/interface/core/SemanticEventChainInterfaces.h>

namespace armarx
{
    /**
     * @class SemanticEventChainGeneratorPropertyDefinitions
     * @brief
     * @ingroup Observers
     */
    class SemanticEventChainGeneratorPropertyDefinitions:
            public ComponentPropertyDefinitions
    {
    public:
        SemanticEventChainGeneratorPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ViconCSVFile", "Path to Vicon CSV file - absolute or relative to ArmarXDataPaths");
        }
    };

    /**
     * @class SemanticEventChainGenerator
     * @brief A brief description
     *
     * Detailed Description
     */
    class SemanticEventChainGenerator :
        virtual public armarx::Component,
            virtual public hierarchicalsegmentation::SemanticEventChainGeneratorInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SemanticEventChainGenerator";
        }


        // SemanticEventChainGeneratorInterface interface
        memoryx::SECKeyFrameMap calculateSEC(const hierarchicalsegmentation::StringList &viconCSVString, const Ice::Current &c = Ice::Current());

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        memoryx::PersistentObjectClassSegmentBasePrx objectClassSegment;
        memoryx::PriorKnowledgeInterfacePrx priorKnowledge;



        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::ObjectKeyframes calcKeyframes(const std::string &MMMFilePath, const Ice::Current &);

        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::Keyframes getXPositions(const std::string &, const std::string &, const Ice::Current &);

        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::ObjectKeyframes calcKeyframesWithConfig(const std::string &, const hierarchicalsegmentation::DoubleList &, const Ice::Current &);

        // SemanticEventChainGeneratorInterface interface


        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::KeyframesWithWorldStateVec getWorldStates(const std::string &, const std::string &objectName, const hierarchicalsegmentation::DoubleList &, const Ice::Current &)
        {
            throw NotImplementedYetException();
        }

        // SemanticEventChainGeneratorInterface interface
    public:
        hierarchicalsegmentation::KeyframesWithWorldStateVec calcWorldStatesAtFrames(const std::string &, const hierarchicalsegmentation::DoubleList &, const hierarchicalsegmentation::DoubleList &params, const Ice::Current &)
        {
            throw NotImplementedYetException();
        }
    };
}

#endif

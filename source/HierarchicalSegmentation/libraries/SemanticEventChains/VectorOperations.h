#ifndef HELPERS_H
#define HELPERS_H

#include <ArmarXCore/core/exceptions/Exception.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstring>
#include <limits>

#include <cstdio>
#include <cstdlib>
#include <map>

//#include "defines.h"
//#include "exception.h"

//#include "vec.h"

namespace armarx
{
typedef std::map<double, double > DoubleMap;



static const double J_EPS = 1.2; // TODO: get rid of this!
static const int J_LINE_BUF_SIZE = 1024*1024;


template<class T> T fromString(const std::string& s)
{
     std::istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

template <class T> std::ostream & operator << (std::ostream &out, const std::vector<T>& v) {
    out << "VECTOR (" << v.size() << "): ";
    for(typename std::vector<T>::const_iterator i=v.begin(); i!=v.end(); ++i) {
		out << *i;
        if (i+1 != v.end())
            out << ", ";
    }
    out << ";";
	return out;
}

template <class T> std::vector<T> operator -(const std::vector<T> &v1, const std::vector<T> &v2) {
    std::vector<T> res(std::min(v1.size(),v2.size()), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = v1[j] - v2[j];
		++j;
	}
	return res;
} 

template <class T> std::vector<T> operator +(const std::vector<T> &v1, const std::vector<T> &v2) {
    std::vector<T> res(std::min(v1.size(),v2.size()), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = v1[j] + v2[j];
		++j;
	}
	return res;
}

template <class T> std::vector<T>& operator +=(std::vector<T> &v1, const std::vector<T> &v2) {
	int j = 0;
    for(typename std::vector<T>::iterator i=v1.begin(); i!=v1.end(); ++i)  {
		*i += v2[j];
		++j;
	}
	return v1;
}


template <class T> std::vector<T>& operator -=(std::vector<T> &v1, const std::vector<T> &v2) {
	int j = 0;
    for(typename std::vector<T>::iterator i=v1.begin(); i!=v1.end(); ++i)  {
		*i -= v2[j];
		++j;
	}
	return v1;
}

template <class T> std::vector<T>& operator *=(std::vector<T> &v1, double c) {
	int j = 0;
    for(typename std::vector<T>::iterator i=v1.begin(); i!=v1.end(); ++i)  {
		*i *= c;
		++j;
	}
	return v1;
}



template <class T> std::vector<T>& operator /=(std::vector<T> &v1, double c) {
	int j = 0;
    for(typename std::vector<T>::iterator i=v1.begin(); i!=v1.end(); ++i)  {
		*i /= c;
		++j;
	}
	return v1;
}

template <class T> std::vector<T> operator *(double c, const std::vector<T> &v) {
    std::vector<T> res(v.size(), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = c*v[j];
		++j;
	}
	return res;
}

template <class T> double operator *(const std::vector<T> &v, const std::vector<T> &v2) {
    double res = 0;
    if(v.size() != v2.size())
        throw LocalException("vectors do not match in size: ") << v.size() << " vs. " << v2.size();
    int j = 0;
    for(typename std::vector<T>::const_iterator i=v.begin(); i!=v.end(); ++i)  {
        res += *i * v2[j];
        ++j;
    }
    return res;
}


template <class T> std::vector<T> operator +(const std::vector<T> &v, double s) {
    std::vector<T> res(v.size(), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = v[j]+s;
		++j;
	}
	return res;
}

template <class T> std::vector<T> operator -(const std::vector<T> &v, double s) {
    std::vector<T> res(v.size(), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = v[j]-s;
		++j;
	}
	return res;
}

template <class T> double vecLength(const std::vector<T> &v) {
    double result = 0.0;

    for(typename std::vector<T>::const_iterator it = v.begin(); it != v.end(); it++)  {
        result += *it * *it;
    }
    return sqrt(result);
}

template <class T> std::vector<T> normalizedVec(const std::vector<T> &v) {
    double length = vecLength(v);
    std::vector<T> result = v;
    result /= length;
    return result;
}

template <class T> std::vector<T> operator /(const std::vector<T> &v, double c) {
    std::vector<T> res(v.size(),  T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = v[j]/c;
		++j;
	}
	return res;
}

template <class T> std::vector<T> abs(const std::vector<T> &v) {
    std::vector<T> res(v.size(), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
        *i = fabs(v[j]);
		++j;
	}
	return res;
}

template <class T> std::vector<T> max(const std::vector<T> &v1, const std::vector<T> &v2) {
    std::vector<T> res(std::min(v1.size(),v2.size()), T());
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
        *i = std::max(v1[j], v2[j]);
		++j;
	}
	return res;
}

template <class T> T max(const std::vector<T> &v1) {
    T maxValue = std::numeric_limits<T>::min();
    for(typename std::vector<T>::const_iterator i=v1.begin(); i!=v1.end(); ++i)  {
        maxValue = std::max(maxValue, *i);

    }
    return maxValue;
}

template <class T> std::vector<T> min(const std::vector<T> &v1, const std::vector<T> &v2) {
    std::vector<T> res(std::min(v1.size(),v2.size()));
	int j = 0;
    for(typename std::vector<T>::iterator i=res.begin(); i!=res.end(); ++i)  {
		*i = std::min(v1[j], v2[j]);
		++j;
	}
	return res;
}


inline std::vector<double>  turn2DVector(const std::vector<double>  &v, double alpha) {
    std::vector<double>  res;
	res.resize(2);
	res[0] = cos(alpha)*v[0] - sin(alpha)*v[1];
	res[1] = sin(alpha)*v[0] + cos(alpha)*v[1];
	return res;
}


inline void pol2cart(double phi, double r, double &x, double &y) {
	x = r*cos(phi);
	y = r*sin(phi);
}

inline std::vector<double>  pol2cart(const std::vector<double>  &pol) {
    std::vector<double>  res;
	res.resize(2);
	pol2cart(pol[0],pol[1], res[0],res[1]);
	return res;
}














inline static double norm2(const std::vector<double>  &v) {
	double res = 0.0;
	for (unsigned int i=0; i<v.size(); ++i) 
		res += v[i]*v[i];
	return sqrt(res);
}

inline static double weightedNorm2(const std::vector<double>  &v, const std::vector<double>  &weights) {
	double res = 0.0;
	for (unsigned int i=0; i<v.size(); ++i) 
		res += weights[i]*v[i]*v[i];
	return sqrt(res);
}

inline static bool getNextLine(FILE *fp, char *buf) {
    while(!feof(fp))
	{
		fgets(buf, J_LINE_BUF_SIZE, fp);
		
		if(strlen(buf) > 1)
			return true;
    }
    return false;
}

}

#endif // HELPERS_H

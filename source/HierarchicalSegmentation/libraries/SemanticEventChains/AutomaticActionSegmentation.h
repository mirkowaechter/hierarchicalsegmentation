/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef AUTOMATICACTIONSEGMENTATION_H
#define AUTOMATICACTIONSEGMENTATION_H

//#include <dmp/general/helpers.h>
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <string>
#include <vector>
#include <map>
#include <ArmarXCore/core/logging/Logging.h>

#include <MemoryX/interface/memorytypes/MemoryEntities.h>



namespace armarx
{
//    struct Relation
//    {
//        Relation(std::string name, std::string object1, std::string object2, bool negated);
//        std::string name;
//        std::string object1;
//        std::string object2;
//        bool negated;
//    };

//    struct WorldState
//    {
//        int containsRelation(std::string object1, std::string object2) const;
//        int addRelations(const std::vector<Relation>& newRelations);
//        std::vector<Relation> relations;
//    };

//    class KeyFrame
//    {
//    public:
//        KeyFrame();
//        int index;
//        memoryx::SECObjectRelationsBasePtr worldState;
////        WorldState worldState;
//    };
//    typedef std::map<int, KeyFrame> SECKeyFrameMap;
    typedef std::map<std::string, memoryx::ObjectClassBasePtr > ObjectClassMap;

    typedef std::vector<std::string> StringList;

    class AutomaticActionSegmentation :
            public Logging
    {
    public:
        enum ImageType{
            eSVG,
            eEPS
        };

        AutomaticActionSegmentation(double distanceThreshold, double distanceChangeThreshold, int minConsecutiveKeyframes, int mergeThreshold, int fps );
        void setMergeKeyFrames(bool __mergeKeyFrames);
        void setAgents(StringList agents);
        void setObjects(const memoryx::ObjectClassList &objects);

        void calc(std::vector<std::vector<std::vector<double> > > & _3DPositions, std::map<std::string, std::vector<int> > &__markerGroups, bool storeStateChangesOnly = false);
        void plot(std::vector<double> frames, ImageType imageType);
        void _findKeyFrames();

        static void MergeKeyFrames(memoryx::SECKeyFrameMap &keyframes, float mergeThreshold);
        static void MergeByAgents(memoryx::SECKeyFrameMap &keyframes, const memoryx::ObjectClassList &objects);

        memoryx::SECKeyFrameMap calcCumulativeWorldStates();

        // aux
        static std::vector<double> calculateDistances(std::vector<std::vector<std::vector<double> > > positions, int object1Index, int object2Index);
        static std::vector<double> deriv(const std::vector<double> &vec, float fps = 100);
        static std::vector<double> getColumn(const std::vector<std::vector<double> > &matrix, unsigned int colIndex);

        memoryx::SECKeyFrameMap getKeyFrames() const;

        bool getXMLString(std::string &XMLOutput) const;
        std::string getXMLString() const;
        bool writeAsXML(std::string xmlFilepath) const;

        double distanceThreshold;
        double distanceChangeThreshold;
        //minimum framecount between 2 keyframes
        int minConsecutiveKeyframes;
        int mergeThreshold;
        double fps;
    protected:
        void _findKeyFrames(std::string object1, std::string object2, memoryx::SECKeyFrameMap &outputKeyFrames);
        void _calculateWorldStates();
        int _findNextTouchingTransition(const std::vector<double>& distances, const std::vector<double> &distanceChange, int startKeyFrame);
        int _findNextNonTouchingTransition(const std::vector<double> &distances, const std::vector<double> &distanceChange, int startKeyFrame);

        bool _insertKeyFramesInXMLNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *topNode) const;
        bool _insertObjectListInXMLNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *topNode) const;
        bool _insertObjectRelationsInXMLNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *keyFrameNode, const memoryx::SECKeyFrameBasePtr &keyFrame) const;

        memoryx::ObjectClassBasePtr getObject(const std::string & objectName) const;
    private:
        // first dim is framenumber, 2nd dim is marker and 3rd dim is 3d pos
        std::vector<std::vector<std::vector<double> > > __3DPositions;
        std::map<std::string, std::vector<double> > __interestingDistancePlots;
        memoryx::SECKeyFrameMap __keyframes;
        std::map<std::string, std::vector<int> > __markerGroups;
        ObjectClassMap __objects;
        StringList __agents;
        bool __mergeKeyFrames;
    };


}

#endif // AUTOMATICACTIONSEGMENTATION_H

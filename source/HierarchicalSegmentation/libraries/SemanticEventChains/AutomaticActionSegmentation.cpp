/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "AutomaticActionSegmentation.h"
#include "GnuPlotWrapper.h"
#include "VectorOperations.h"

//#include <dmp/general/helpers.h>

#include <set>
#include <algorithm>

#include <boost/foreach.hpp>
#include <boost/type_traits.hpp>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECObjectRelations.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECKeyFrame.h>

#include <ArmarXCore/core/rapidxml/rapidxml_print.hpp>


using namespace rapidxml;
using namespace armarx;
using namespace memoryx;




AutomaticActionSegmentation::AutomaticActionSegmentation(double distanceThreshold, double distanceChangeThreshold, int minConsecutiveKeyframes, int mergeThreshold, int fps):
    distanceThreshold(distanceThreshold),
    distanceChangeThreshold(distanceChangeThreshold),
    minConsecutiveKeyframes(minConsecutiveKeyframes),
    mergeThreshold(mergeThreshold),
    fps(fps),
    __mergeKeyFrames(true)
{
    assert(minConsecutiveKeyframes > 0);
    assert(fps >  0);
    setTag("AAS");
}

void AutomaticActionSegmentation::setMergeKeyFrames(bool mergeKeyFrames)
{
    this->__mergeKeyFrames = mergeKeyFrames;
}

void AutomaticActionSegmentation::setAgents(StringList agents)
{
    this->__agents = agents;
}

void AutomaticActionSegmentation::setObjects(const ObjectClassList &objects)
{
    __objects.clear();
    ObjectClassList::const_iterator it = objects.begin();
    for(; it != objects.end(); it++)
    {
        __objects[(*it)->getName()] = *it;
    }
}

void AutomaticActionSegmentation::calc(std::vector<std::vector<std::vector<double> > > &_3DPositions, std::map<std::string, std::vector<int> > &markerGroups, bool storeStateChangesOnly)
{
    __3DPositions = _3DPositions;
    this->__markerGroups = markerGroups;
    _findKeyFrames();

    if(__mergeKeyFrames)
        MergeKeyFrames(__keyframes, mergeThreshold);
    if(!storeStateChangesOnly)
        __keyframes = calcCumulativeWorldStates();
}

void AutomaticActionSegmentation::plot(std::vector<double> frames, ImageType imageType)
{
    //    std::vector<double> frames = getColumn(__3DPositions, 0);
    std::vector<std::vector<double> > plots;
    std::vector<std::string> titles;
    //    frames.resize(450, 0);
    plots.push_back(frames);

    titles.push_back("frames");
    std::map<std::string, std::vector<double> >::iterator it1 = __interestingDistancePlots.begin();
    for(; it1 != __interestingDistancePlots.end(); it1++)
    {
        plots.push_back(it1->second);
        titles.push_back(it1->first);
        //        plots.push_back(deriv(it1->second, fps));
        //        titles.push_back(it1->first+"Deriv");
    }
    std::stringstream add;
    add << "set xrange [0.00:"<< frames.size() << "]\n";
    add << "set yrange [0:500]\n";
    add << "set key left top\n";
    add << " set xlabel 'Frame number'\n" ;
    add << " set ylabel 'Marker distance (mm)'\n";
    SECKeyFrameMap::iterator it = __keyframes.begin();
    int i=1;
    for(; it != __keyframes.end(); it++)
    {
        add << "set arrow from " << it->first+*frames.begin() << ",graph(0,0) to " << it->first+*frames.begin() << ",graph(1,1) nohead lt 0;\n";
        i++;
    }
    add << "set arrow from "<< *frames.begin() <<"," << distanceThreshold << " to "<< *frames.rbegin() << "," <<distanceThreshold << " nohead lt 5 lc rgb 'black';\n";

    // svg
    if(imageType == eSVG)
    {
        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
        add << "set output 'plots/comparison.svg'\n";
    }
    else
    {
        //    // eps
        add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
            <<" set output 'plots/aas.eps'";
    }
    GnuPlotWrapper::plotTogether(plots,titles,"plots/aas_%d.gp", true, add.str());
}

void AutomaticActionSegmentation::_findKeyFrames()
{
    __keyframes.clear();
    std::map<std::string, std::vector<int> >::iterator i1 = __markerGroups.begin();
    for (; i1 != __markerGroups.end(); ++i1) {
        std::map<std::string, std::vector<int> >::iterator i2 = i1;
        for( ; i2 != __markerGroups.end(); ++i2) {
            //            SECKeyFrameMap keyframes;
            _findKeyFrames(i1->first, i2->first,__keyframes);
            //            __keyframes.insert(keyframes.begin(), keyframes.end());
        }
    }
}

void AutomaticActionSegmentation::MergeKeyFrames( memoryx::SECKeyFrameMap& keyframes, float mergeThreshold)
{
    ARMARX_VERBOSE_S << "Merging key frames...";
    int mergeCandidate = -1; // index of the main SECKeyFrameBasePtr, that survives a merge
    typedef std::pair<int,int> MergePair;
    std::set<MergePair > eraseList;
    auto it = keyframes.rbegin();
    for(; it != keyframes.rend(); it++)
    {
        auto itPrev = it;
        itPrev++;
        if(itPrev == keyframes.rend())
            break;
        int difference = it->first - itPrev->first;
        ARMARX_VERBOSE_S << "Checking " << it->first << " and " << itPrev->first;
        if(difference <= mergeThreshold)
        {
            ARMARX_VERBOSE_S << "Below threshold";
            if(mergeCandidate == -1)
            {
                ARMARX_VERBOSE_S << "Merge survivor will be " << it->first;
                mergeCandidate = it->first;
            }
            eraseList.insert(std::make_pair(mergeCandidate, itPrev->first));

        }
        else
        {
            //            if(mergeCandidate != -1)
            //            {
            //                ARMARX_VERBOSE << "Erasing " << eraseList.size() << " keyframes" << std::endl;
            //                BOOST_FOREACH(int elem, eraseList)
            //                {
            //                    ARMARX_VERBOSE << elem <<", ";
            //                    keyframes[mergeCandidate].worldState->addRelations(keyframes[mergeCandidate].worldState->relations);
            //                    keyframes.erase(elem);
            //                }
            //                ARMARX_VERBOSE << std::endl;
            //                eraseList.clear();
            //                it = std::reverse_iterator<SECKeyFrameMap::iterator>(keyframes.find(mergeCandidate));
            //                BOOST_AUTO(itPrev, it);
            //                itPrev++;
            //                ARMARX_VERBOSE << "mergeCandidate: " << mergeCandidate << " rnext: " << itPrev->first;
            //            }
            mergeCandidate = -1;
        }
    }


    ARMARX_VERBOSE_S << "Erasing " << eraseList.size() << " keyframes" << std::endl;

    BOOST_FOREACH(MergePair elem, eraseList)
    {
        ARMARX_VERBOSE_S << elem.second << ", ";
        SECObjectRelationsPtr rels = SECObjectRelationsPtr::dynamicCast(keyframes[elem.first]->worldState->ice_clone());
        rels->addRelations(keyframes[elem.second]->worldState->relations);
        rels->addRelations(keyframes[elem.first]->worldState->relations);
        keyframes[elem.first]->worldState = rels;
        keyframes.erase(elem.second);
    }
    //    ARMARX_VERBOSE << std::endl;
    //    eraseList.clear();
    //    it = std::reverse_iterator<SECKeyFrameMap::iterator>(keyframes.find(mergeCandidate));
    //    BOOST_AUTO(itPrev, it);
    //    itPrev++;
    //    ARMARX_VERBOSE << "mergeCandidate: " << mergeCandidate << " rnext: " << itPrev->first;


    ARMARX_VERBOSE_S << std::endl;

}

void AutomaticActionSegmentation::MergeByAgents(memoryx::SECKeyFrameMap &keyframes, const memoryx::ObjectClassList &objects)
{
//    int mergeCandidate = -1; // index of the main SECKeyFrameBasePtr,      survives a merge
//    std::set<int> eraseList;
//    BOOST_AUTO(it, keyframes.rbegin());
//    for(; it != keyframes.rend(); it++)
//    {
//        const memoryx::SECKeyFrameBasePtr & SECKeyFrameBasePtr = it->second;

//        bool keyFrameWithAgent = false;
//        for(std::vector<SECRelationBasePtr>::iterator itRel = SECKeyFrameBasePtr.worldState->relations.begin(); itRel != SECKeyFrameBasePtr.worldState->relations.end(); itRel++)
//        {
//            SECRelationPtr relation = *it;
//            relation->objects1->
//            if(std::find(objects.begin(), __agents.end(), itRel->object1) != __agents.end() || std::find(__agents.begin(), __agents.end(), itRel->object2) != __agents.end())
//            {
//                keyFrameWithAgent = true;
//                break;
//            }
//        }

//        if(!keyFrameWithAgent)
//        {
//            eraseList.insert(it->first);
//        }
//        else
//        {
//            mergeCandidate = it->first;
//            if(mergeCandidate != -1)
//            {
//                ARMARX_VERBOSE << "Erasing " << eraseList.size() << " keyframes" << std::endl;
//                BOOST_FOREACH(int elem, eraseList)
//                {
//                    ARMARX_VERBOSE << elem <<", ";
//                    __keyframes[mergeCandidate].worldState->relations.insert(__keyframes[mergeCandidate].worldState->relations.begin(), __keyframes[elem].worldState->relations.begin(), __keyframes[elem].worldState->relations.end());
//                    __keyframes.erase(elem);
//                }
//                ARMARX_VERBOSE << std::endl;
//                eraseList.clear();
//            }
//            mergeCandidate = -1;
//        }
//    }

//    ARMARX_VERBOSE << std::endl;
}

SECKeyFrameMap AutomaticActionSegmentation::calcCumulativeWorldStates()
{
    SECKeyFrameMap result;
    SECKeyFrameBasePtr currentKeyFrame = new SECKeyFrame();
    std::map<std::string, std::vector<int> >::iterator i1 = __markerGroups.begin();
    for (; i1 != __markerGroups.end(); ++i1) {
        std::map<std::string, std::vector<int> >::iterator i2 = i1;
        for(i2++ ; i2 != __markerGroups.end(); ++i2) {
            ObjectClassList objects1;
            objects1.push_back(getObject(i1->first));
            ObjectClassList objects2;
            objects2.push_back(getObject(i2->first));
//            currentKeyFrame->worldState->relations.push_back(new Relations::TouchingRelation(objects1, objects2));
            currentKeyFrame->worldState->addRelation(new Relations::NoConnectionRelation(objects1, objects2));
//            ARMARX_DEBUG_S << "Adding relation: " << (*currentKeyFrame->worldState->relations.rbegin())->output();
            //            currentKeyFrame.worldState->relations.push_back(Relation("Touching", i1->first, i2->first, true));
            //            currentKeyFrame.worldState->relations.push_back(Relation("Touching", i1->first, i2->first, true));
        }
    }

    auto it = __keyframes.begin();
    for(; it != __keyframes.end(); it++)
    {
        const SECKeyFrameBasePtr& selectedKeyFrame = it->second;
        currentKeyFrame->index = selectedKeyFrame->index;
        ARMARX_IMPORTANT << selectedKeyFrame->index << " vs. " << it->first;
        currentKeyFrame->worldState->addRelations(selectedKeyFrame->worldState->relations);
//        for(unsigned int i=0; i < selectedKeyFrame->worldState->relations.size(); i++)
//        {
//            currentKeyFrame->worldState->addRelation(selectedKeyFrame->worldState->relations.at(i));
//            //            int relIndex = currentKeyFrame.worldState->containsRelation(selectedKeyFrame.worldState->relations.at(i).object1, selectedKeyFrame.worldState->relations.at(i).object2);
//            //            if(relIndex != -1)
//            //                currentKeyFrame.worldState->relations.at(relIndex) = selectedKeyFrame.worldState->relations.at(i);
//            //            else
//            //                currentKeyFrame.worldState->relations.push_back(selectedKeyFrame.worldState->relations.at(i));
//        }
        result[it->first] = SECKeyFrameBasePtr::dynamicCast(currentKeyFrame->ice_clone());
    }
    return result;
}

void AutomaticActionSegmentation::_findKeyFrames(std::string object1, std::string object2, SECKeyFrameMap& outputKeyFrames)
{
    if(object1 == object2)
        return;
    ARMARX_VERBOSE << "Checking objects: "<< object1 << " and " << object2 << std::endl;
    std::vector<int> obj1Markers = __markerGroups[object1];
    std::vector<int> obj2Markers = __markerGroups[object2];
    std::map<int, std::map<int,std::vector<double> > > distances;
    std::map<int, std::map<int,std::vector<double> > > distanceChanges;

    ObjectClassList objects1;
    objects1.push_back(getObject(object1));
    ObjectClassList objects2;
    objects2.push_back(getObject(object2));

    // Calculate the distances between all objects
    std::vector<int>::iterator i1 = obj1Markers.begin();
    for (; i1 != obj1Markers.end(); ++i1) {
        std::vector<int>::iterator i2 = obj2Markers.begin();
        for( ; i2 != obj2Markers.end(); ++i2) {
            distances[*i1][*i2] = calculateDistances(__3DPositions, *i1, *i2 );
            distanceChanges[*i1][*i2] = deriv(distances[*i1][*i2], fps);
        }
    }


    unsigned int currentFrame = 0;
    bool lastKeyframeWasTouching = false;
    std::vector<int>::iterator i2;
    while(currentFrame < __3DPositions.size())
    {
        int keyframeIndex = -1;

        if(lastKeyframeWasTouching)
            keyframeIndex = _findNextNonTouchingTransition(distances[*i1][*i2], distanceChanges[*i1][*i2],currentFrame);
        else
        {
            i1 = obj1Markers.begin();
            for (; i1 != obj1Markers.end() && keyframeIndex == -1; ++i1) {
                i2 = obj2Markers.begin();
                for( ; i2 != obj2Markers.end(); ++i2) {
                    keyframeIndex = _findNextTouchingTransition(distances[*i1][*i2], distanceChanges[*i1][*i2], currentFrame);
                    if(keyframeIndex != -1)
                        break;
                }
                if(keyframeIndex != -1)
                    break;
            }
        }
        if(keyframeIndex!=-1)
        {
            lastKeyframeWasTouching = !lastKeyframeWasTouching;
//            SECKeyFrameBasePtr SECKeyFrameBasePtr;
//            SECKeyFrameBasePtr.index = keyframeIndex;

            //            keyframes.insert(std::make_pair(keyframeIndex,  SECKeyFrameBasePtr));
            SECKeyFrameMap::iterator it = outputKeyFrames.find(keyframeIndex);
            if(it == outputKeyFrames.end())
                outputKeyFrames[keyframeIndex] = new SECKeyFrame();
            outputKeyFrames[keyframeIndex]->index = keyframeIndex;
            std::vector<SECRelationBasePtr> newRelations;
            if(lastKeyframeWasTouching)
                newRelations.push_back(new Relations::TouchingRelation(objects1, objects2));
            else
                newRelations.push_back(new Relations::NoConnectionRelation(objects1, objects2));
            outputKeyFrames[keyframeIndex]->worldState->addRelations(newRelations);
            //            int relIndex = outputKeyFrames[keyframeIndex].worldState->containsRelation(object1, object2);
            //            if(relIndex != -1)
            //                outputKeyFrames[keyframeIndex].worldState->relations.at(relIndex) = ;
            //            else
            //                outputKeyFrames[keyframeIndex].worldState->relations.push_back(Relation("Touching", object1, object2, !lastKeyframeWasTouching));
            //            outputKeyFrames[keyframeIndex].worldState->relations.push_back(Relation("Touching", object1, object2, !lastKeyframeWasTouching));

            __interestingDistancePlots[object1+" <-> " + object2] = (distances[*i1][*i2]);
            if(!lastKeyframeWasTouching)
                ARMARX_VERBOSE << "NonTouchingTransition: ";
            else
                ARMARX_VERBOSE << "TouchingTransition: ";
            ARMARX_VERBOSE << "found KeyFrame at: " << keyframeIndex << " for objects: " << object1 << " and " << object2 << std::endl;
            currentFrame = keyframeIndex+minConsecutiveKeyframes;
        }

        if(keyframeIndex == -1) // no SECKeyFrameBasePtr for any marker
            currentFrame = __3DPositions.size();
    }
}

void AutomaticActionSegmentation::_calculateWorldStates()
{
}

int AutomaticActionSegmentation::_findNextTouchingTransition(const std::vector<double> &distances, const std::vector<double> &distanceChanges, int startKeyFrame)
{
    //    std::vector<double> distancesDeriv = deriv(distances, fps);
    int consecutiveFrames = 0;
    int keyframeTemp = -1;
    for (unsigned int frame = startKeyFrame; frame < distances.size(); ++frame) {
        //        double distance = distances.at(frame) ;
        //        double distanceChange = distanceChanges.at(frame);
        if(distances.at(frame) <= distanceThreshold
                && fabs(distanceChanges.at(frame))  <= distanceChangeThreshold)
        {
            if(consecutiveFrames == 0)
                keyframeTemp = frame;
            consecutiveFrames++;
        }
        else{

            consecutiveFrames = 0;
        }
        if(consecutiveFrames >= minConsecutiveKeyframes)
        {
            return keyframeTemp;
        }
    }
    return -1;
}

int AutomaticActionSegmentation::_findNextNonTouchingTransition(const std::vector<double> &distances, const std::vector<double> &distanceChanges, int startKeyFrame)
{
    //    std::vector<double> distancesDeriv = deriv(distances, fps);
    int consecutiveFrames = 0;
    int keyframeTemp = -1;
    for (unsigned int frame = startKeyFrame; frame < distances.size(); ++frame) {
        double distance = distances.at(frame) ;
        double distanceChange = distanceChanges.at(frame);
        if(distance > distanceThreshold
                || fabs(distanceChange)  > distanceChangeThreshold)
        {
            if(consecutiveFrames == 0)
                keyframeTemp = frame;
            consecutiveFrames++;
        }
        else{
            consecutiveFrames = 0;
        }
        if(consecutiveFrames >= minConsecutiveKeyframes)
        {
            return keyframeTemp;
        }
    }
    return -1;
}




std::vector<double> AutomaticActionSegmentation::calculateDistances(std::vector<std::vector<std::vector<double> > > positions, int object1Index, int object2Index)
{
    std::vector<double> distances;
    for (unsigned int rowIndex = 0; rowIndex < positions.size(); ++rowIndex) {
        const std::vector<std::vector<double> >& row = positions.at(rowIndex);
        distances.push_back(vecLength(row.at(object1Index) - row.at(object2Index)));
    }
    return distances;
}

std::vector<double> AutomaticActionSegmentation::deriv(const std::vector<double>& vec, float fps)
{
    std::vector<double> result;
    if(vec.size() < 2)
        return result;

    result.resize(vec.size());
    result.at(0) = vec.at(1) - vec.at(0);
    *result.rbegin() = (*vec.rbegin()) - (*(vec.rbegin()-1));
    for (unsigned int frame = 1; frame < vec.size()-1; ++frame) {
        const double& before = vec.at(frame-1);
        const double& after = vec.at(frame+1);
        double& deriv = result.at(frame);
        deriv = (after - before)*fps;

    }
    return result;
}


std::vector<double> AutomaticActionSegmentation::getColumn(const std::vector<std::vector<double> > & matrix, unsigned int colIndex)
{
    std::vector<double> result;
    for (unsigned int row = 0; row < matrix.size(); ++row) {
        result.push_back(matrix.at(row).at(colIndex));
    }
    return result;
}

SECKeyFrameMap AutomaticActionSegmentation::getKeyFrames() const
{
    return __keyframes;
}

char* intToAllocatedCharString(xml_document<> &doc, int value)
{
    std::stringstream valueStream;
    valueStream << value;
    return doc.allocate_string(valueStream.str().c_str());
}

bool AutomaticActionSegmentation::getXMLString(std::string& XMLOutput) const
{
    xml_document<> doc;
    char *node_name = doc.allocate_string("GraphML");
    xml_node<> *node = doc.allocate_node(node_element, node_name);
    doc.append_node(node);
    xml_attribute<> *attr = doc.allocate_attribute("keyFrameCount", intToAllocatedCharString(doc, __keyframes.size()));
    node->append_attribute(attr);
    bool result = true;
    result &= _insertObjectListInXMLNode(doc,node);
    result &= _insertKeyFramesInXMLNode(doc, node);
    std::stringstream xmlStream;
    xmlStream << doc;
    XMLOutput = xmlStream.str();
    return result;
}

std::string AutomaticActionSegmentation::getXMLString() const
{
    std::string result;
    getXMLString(result);
    return result;
}


bool AutomaticActionSegmentation::writeAsXML(std::string xmlFilepath) const
{
    std::ofstream file(xmlFilepath.c_str());
    if(!file.is_open())
    {
        ARMARX_VERBOSE << "Could not open xml state file: " << xmlFilepath;
        return false;
    }
    xml_document<> doc;
    char *node_name = doc.allocate_string("GraphML");
    xml_node<> *node = doc.allocate_node(node_element, node_name);
    doc.append_node(node);
    xml_attribute<> *attr = doc.allocate_attribute("keyFrameCount", intToAllocatedCharString(doc, __keyframes.size()));
    node->append_attribute(attr);
    std::string xmlString;
    bool result  = getXMLString(xmlString);

    file << "<?xml version='1.0' encoding='UTF-8'?>" << std::endl;
    file <<  xmlString;
    file.close();

    return result;
}


bool AutomaticActionSegmentation::_insertKeyFramesInXMLNode(xml_document<> &doc, xml_node<> *topNode) const
{
    SECKeyFrameMap::const_iterator it = __keyframes.begin();
    for(; it != __keyframes.end(); it++)
    {
        const SECKeyFrameBasePtr& keyFrameBasePtr = it->second;
        char *node_name = doc.allocate_string("KeyFrame");
        xml_node<> *node = doc.allocate_node(node_element, node_name);
        xml_attribute<> *attr = doc.allocate_attribute("ID", intToAllocatedCharString(doc, keyFrameBasePtr->index));
        node->append_attribute(attr);
        topNode->append_node(node);
        _insertObjectRelationsInXMLNode(doc, node, keyFrameBasePtr);
    }

    return true;

}

bool AutomaticActionSegmentation::_insertObjectListInXMLNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *topNode) const
{
    std::map<std::string, std::vector<int> >::const_iterator it = __markerGroups.begin();
    for(; it != __markerGroups.end(); it++)
    {
        char *node_name = doc.allocate_string("Object");
        xml_node<> *node = doc.allocate_node(node_element, node_name, doc.allocate_string(it->first.c_str()));
        topNode->append_node(node);
    }

    return true;
}

bool AutomaticActionSegmentation::_insertObjectRelationsInXMLNode(rapidxml::xml_document<> &doc, rapidxml::xml_node<> *keyFrameNode, const SECKeyFrameBasePtr & keyFrameBasePtr) const
{
    const SECObjectRelationsBasePtr& worldState = keyFrameBasePtr->worldState;
    SECRelationList::const_iterator it = worldState->relations.begin();
    for(; it != worldState->relations.end(); it++)
    {
        char *node_name = doc.allocate_string("Edge");
        xml_node<> *node = doc.allocate_node(node_element, node_name);
        const SECRelationPtr& relation = SECRelationPtr::dynamicCast(*it);
        xml_attribute<> *attrObj1 = doc.allocate_attribute("source", doc.allocate_string(relation->objects1ToString().c_str()));
        node->append_attribute(attrObj1);
        xml_attribute<> *attrObj2 = doc.allocate_attribute("target", doc.allocate_string(relation->objects2ToString().c_str()));
        node->append_attribute(attrObj2);
        xml_attribute<> *attrRelation = NULL;
        //        if(relation.name)
        //            attrRelation = doc.allocate_attribute("relation", "NoConnection");
        //        else
        attrRelation = doc.allocate_attribute("relation", doc.allocate_string(relation->getName().c_str()));
        node->append_attribute(attrRelation);
        keyFrameNode->append_node(node);
    }
    return true;
}

ObjectClassBasePtr AutomaticActionSegmentation::getObject(const std::string &objectName) const
{
    ObjectClassMap::const_iterator it = __objects.find(objectName);
    if(it != __objects.end())
        return it->second;
    else
        throw LocalException("Could not find object in map with name: ") << objectName;
    return NULL;
}

//Relation::Relation(std::string name, std::string object1, std::string object2, bool negated):
//    name(name), object1(object1), object2(object2), negated(negated)
//{
//}




//int WorldState::containsRelation(std::string object1, std::string object2) const
//{
//    std::vector<Relation>::const_iterator it = relations.begin();
//    int i = 0;
//    for(; it != relations.end(); it++)
//    {
//        if( (it->object1 == object1 && it->object2 == object2)
//                || (it->object1 == object2 && it->object2 == object1) )
//            return i;
//        i++;
//    }
//    return -1;
//}

//int WorldState::addRelations(const std::vector<Relation> &newRelations)
//{
//    int insertCount = 0;
//    for(unsigned int i=0; i < newRelations.size(); i++)
//    {
//        int relIndex = containsRelation(newRelations.at(i).object1, newRelations.at(i).object2);
//        if(relIndex != -1)
//            relations.at(relIndex) = newRelations.at(i);
//        else
//        {
//            relations.push_back(newRelations.at(i));
//            insertCount++;
//        }
//    }
//    return insertCount;
//}


